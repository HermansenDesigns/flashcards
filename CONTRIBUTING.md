# How to contribute to flashcards

## Did you find a bug?

* Ensure the bug was not already reported

## Do you intend to add a new feature or change an existing one?

* Suggest your change by opening an issue (be sure to add the label suggestion) or send an email at hermansendev@gmail.com

## Do you have questions about the source code?

* Join our slack community or send an email at hermansendev@gmail.com

## Do you want to contribute to the flashcard wiki?

* Send an email at hermansendev@gmail.com or suggest a change

Thanks!

Kasper J. Hermansen