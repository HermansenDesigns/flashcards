from typing import List

from controller.controller import Controller
from project.models.deck import Deck


class DeckController(Controller):
    decks: List[Deck]

    def __init__(self):
        self.decks: [Deck] = []
        self.current_deck: Deck

    def create(self, name):
        deck = Deck(name)
        self.decks.append(deck)

    def delete(self, name):
        d: Deck
        for d in self.decks:
            if d.deck_name == name:
                self.decks.remove(d)

    def load(self, name) -> Deck:
        d: Deck
        for d in self.decks:
            if d.deck_name == name:
                return d

    @property
    def list(self) -> [Deck]:
        return [deck for deck in self.decks]
