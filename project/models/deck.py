from project.models.card import Card


class Deck:
    def __init__(self, deck_name=""):
        self.cards: [Card] = []
        self.deck_name = deck_name

    def add(self, card):
        self.cards.append(card)

    def remove(self, card: Card):
        for c in self.cards:
            if c.question == card.question and c.answer == card.answer:
                self.cards.remove(c)

    def __repr__(self):
        return str([card.reveal() for card in self.cards])
